package work.lijian4net.www.demo11.fivemethod;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author lijian
 * @version 1.0
 * @title Demo11
 * @description
 * @create 2024-01-09 9:01
 */
public class Demo11 {
    public static void main(String[] args) {
        //1、集成Thread 调用start方法
//        for (int i=0;i<10;i++){
//            Thread myThread = new MyThread();
//            myThread.start();
//            System.out.println(Thread.currentThread().getName());
//        }

        //2、实现Runnable接口
//        for (int i=0;i<10;i++){
//            Thread myThread = new Thread(new MyRunnable());
//            myThread.start();
//            System.out.println(Thread.currentThread().getName());
//        }

        //3、实现Callable接口
        FutureTask futureTask = new FutureTask(new MyCallable());
        Thread thread = new Thread(futureTask);
        thread.start();
        try {
            Object o = futureTask.get();
            System.out.println(o);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

}


class MyThread extends Thread{
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"运行了,MyThread");
    }
}

class MyRunnable implements Runnable{

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"运行了,MyRunnable");
    }
}

class MyCallable implements Callable{

    @Override
    public Object call() throws Exception {
        System.out.println(Thread.currentThread().getName()+"运行了,MyCallable");
        return true;
    }
}