package work.lijian4net.www.demo1;

public class Demo04Interface {

    public static void main(String[] args) {
        MyInterfacePrivateB.methodStatic1();
        MyInterfacePrivateB.methodStatic2();
        // 错误写法！
//        MyInterfacePrivateB.methodStaticCommon();
    }

}