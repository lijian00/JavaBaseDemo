package work.lijian4net.www.thread;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ThreadPoolDemo {
    public static void main(String[] args) {

        test();
    }

    public static void test(){
        List<String>  dataList = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            dataList.add("第"+i+"条");
        }


        ThreadToolConfig threadToolConfig = new ThreadToolConfig();
        ThreadPoolExecutor threadPoolExecutor = threadToolConfig.commonThreadPool();

        List<CompletableFuture<Void>> completableFutureList = new ArrayList<>();
        List<List<String>> partition = Lists.partition(dataList,20);//按照每个线程处理数据量拆分
//        List<List<String>> partition = new ArrayList<>();
//        List<String> returnList = new ArrayList<>();//存在线程安全问题
        List<String> returnList = new CopyOnWriteArrayList<>();//线程安全
        CountDownLatch countDownLatch = new CountDownLatch(partition.size());
        Long start = System.currentTimeMillis();
        for(List<String> datas : partition){
            completableFutureList.add(CompletableFuture.runAsync(()->{
                countDownLatch.countDown();
                //处理业务逻辑
                datas.forEach(k->{
                    returnList.add(handleMessgae(k));
                });

            },threadPoolExecutor));
        }
        try{
            System.out.println("等待子线程处理中！");
            CompletableFuture.allOf(completableFutureList.toArray(new CompletableFuture[0])).get();
            countDownLatch.await(60L, TimeUnit.SECONDS);
            System.out.println("子线程总数据量："+dataList.size());
            System.out.println("结束，共耗时："+(System.currentTimeMillis()-start));
        }catch (Exception e){
            throw new RuntimeException(e);
        }
        returnList.forEach(k-> System.out.println(k));
    }
    public static String handleMessgae(String s){
        System.out.println(s);
        return String.format("消息【%s】已处理",s);
    }
}
