package work.lijian4net.www.thread;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

//使用spring注解
//@Configuration
public class ThreadToolConfig {

    private static final int DEFAULT_CORE_POOL_SIZE = Runtime.getRuntime().availableProcessors();

    //注入javaBean
//    @Bean("commThreadPool")
    public ThreadPoolExecutor commonThreadPool(){
        return new ThreadPoolExecutor(DEFAULT_CORE_POOL_SIZE,2000,60L, TimeUnit.SECONDS,new SynchronousQueue<>());
    }

}
