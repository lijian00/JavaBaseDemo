package work.lijian4net.www.singleton.test;

import work.lijian4net.www.singleton.doubleCheck.Singleton;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Demo2 {
    public static void main(String[] args) throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException {
        Singleton instance = Singleton.getInstance();
        System.out.println("原本的 singleton 的 hashcode: " + instance.hashCode());

        //克隆
//        Singleton clone = (Singleton) Singleton.getInstance().clone();
//        System.out.println("克隆获取的 singleton 的 hashcode: " + clone.hashCode());

        //序列化
        ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(Singleton.getInstance());
        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bis);
        Singleton serialize = (Singleton) ois.readObject();
        //关闭资源略
        System.out.println("序列化获取的 singleton 的 hashCode: "+ serialize.hashCode());

        //反射
        Constructor<Singleton> declaredConstructor = Singleton.class.getDeclaredConstructor();
        declaredConstructor.setAccessible(true);
        Singleton singleton = declaredConstructor.newInstance();
        System.out.println("反射获取的 singleton 的 hashcode: " + singleton.hashCode());
    }


}
