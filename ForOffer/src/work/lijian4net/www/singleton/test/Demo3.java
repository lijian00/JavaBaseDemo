package work.lijian4net.www.singleton.test;

import work.lijian4net.www.singleton.doubleCheck.Singleton;
import work.lijian4net.www.singleton.safe.SafeSingleton;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Demo3 {
    public static void main(String[] args) throws IOException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, ClassNotFoundException, CloneNotSupportedException {
        SafeSingleton instance = SafeSingleton.getInstance();
        System.out.println("原本的 singleton 的 hashcode: " + instance.hashCode());

        //克隆
        SafeSingleton clone = (SafeSingleton) SafeSingleton.getInstance().clone();
        System.out.println("克隆获取的 singleton 的 hashcode: " + clone.hashCode());

        //序列化
        ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(SafeSingleton.getInstance());
        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bis);
        SafeSingleton serialize = (SafeSingleton) ois.readObject();
        //关闭资源略
        System.out.println("序列化获取的 singleton 的 hashCode: "+ serialize.hashCode());

        //反射
        Constructor<SafeSingleton> declaredConstructor = SafeSingleton.class.getDeclaredConstructor();
        declaredConstructor.setAccessible(true);
        SafeSingleton singleton = declaredConstructor.newInstance();
        System.out.println("反射获取的 singleton 的 hashcode: " + singleton.hashCode());
    }


}
