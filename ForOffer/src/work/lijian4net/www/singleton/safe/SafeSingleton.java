package work.lijian4net.www.singleton.safe;

import java.io.Serializable;

public class SafeSingleton implements Serializable, Cloneable {
    private static final long serialVersionUID = 6125990676610180062L;
    private volatile static SafeSingleton singleton;
    private static boolean isFristCreate = true;//默认是第一次创建

    private SafeSingleton() {
        if (isFristCreate) {
            synchronized (SafeSingleton.class) {
                if (isFristCreate) {
                    isFristCreate = false;
                }
            }
        } else {
            throw new RuntimeException("已然被实例化一次，不能再实例化");
        }
    }

    public void doAction() {
        //TODO 实现你需要做的事
    }

    public static SafeSingleton getInstance() {
        if (singleton == null) {
            synchronized (SafeSingleton.class) {
                if (singleton == null) {
                    singleton = new SafeSingleton();
                }
            }
        }
        return singleton;
    }

    @Override
    public SafeSingleton clone() throws CloneNotSupportedException {
        return singleton;
    }

    private Object readResolve() {
        return singleton;
    }

}
