package work.lijian4net.www.singleton.doubleCheck;

import java.io.Serializable;

public class Singleton implements Serializable {
    /*
   这里的 volatile 必不可少，volatile 关键字可以避免 JVM 指令重排优化。

   因为 instance = new Singleton(); 可以拆分为三步：

   为 singleton 分配内存空间；
   初始化 singleton;
   将 singleton 指向分配的内存空间；
   但由于 JVM 具有指令重排序的特性，执行顺序可能变为 1-3-2。指令重排序在单线程下不会出现问题，但是在 多线程下会导致线程获取一个未初始化的实例。例如：线程 T1 执行了 1 和 3，此时 T2 调用 getInstance() 后发现 singleton 不为空，因此返回 singleton，但是此时的 singleton 还没有被初始化。
   使用 volatile 会禁止 JVM 指令重排，从而保证在多线程下也能正常执行。
    */
    private volatile static Singleton instance = null;
    private Singleton(){}

    public static Singleton getInstance(){
        if(instance == null)
            synchronized (Singleton.class){
                if(instance == null)
                    instance = new Singleton();
            }
        return instance;
    }

    public void doAction(){
        //TODO 实现你需要做的事
    }
}
