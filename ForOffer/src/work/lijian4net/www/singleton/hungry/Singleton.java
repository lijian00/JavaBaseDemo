package work.lijian4net.www.singleton.hungry;

public class Singleton {
    private static Singleton instance = new Singleton();
    private Singleton(){}

    public static Singleton getInstance(){
        return instance;
    }

    public void doAction(){
        //TODO 实现你需要做的事
    }
}
