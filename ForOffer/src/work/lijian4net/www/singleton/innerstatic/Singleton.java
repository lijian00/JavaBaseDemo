package work.lijian4net.www.singleton.innerstatic;


public class Singleton {
    private static class SingletonHolder {
        private static Singleton instance = new Singleton();
    }
    private Singleton() {

    }
    public static Singleton getInstance() {
        return Singleton.SingletonHolder.instance;
    }

    public void doAction(){
        //TODO 实现你需要做的事
    }
}
