package work.lijian4net.www.singleton.lazy;


public class Singleton {
    private static Singleton instance;
    private Singleton(){}

    public static Singleton getInstance(){
        if(instance == null)
            instance = new Singleton();
        return instance;
    }

    public void doAction(){
        //TODO 实现你需要做的事
    }
}
