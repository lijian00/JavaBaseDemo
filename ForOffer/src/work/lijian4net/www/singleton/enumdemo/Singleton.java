package work.lijian4net.www.singleton.enumdemo;

public enum Singleton {
    INSTANCE;
    public void doSomething() {
        System.out.println("doSomething");
    }

}
