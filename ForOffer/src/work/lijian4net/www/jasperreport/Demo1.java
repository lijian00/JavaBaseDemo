package work.lijian4net.www.jasperreport;

import net.sf.jasperreports.engine.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Demo1 {
    public static void main(String[] args) {

    }

    public static void test1(){
        //map作为报表数据源
        Map<String,Object> parameters = new HashMap<String,Object>(16);
        parameters.put("title", "THIS IS TITLE");
        parameters.put("date", new SimpleDateFormat("yyyy-mm-dd").format(new Date()));
        parameters.put("name", "小明");
        parameters.put("age", "18");
        parameters.put("dept", "开发部");
        parameters.put("gender", "男");

        //引入jasper文件。由JRXML模板编译生成的二进制文件，用于代码填充数据
        String jasperPath = Demo1.class.getResource("/")+"/jasper/DemoReport2.jasper";
        FileInputStream isRef = null;
        try {
            isRef = new FileInputStream(new File(jasperPath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
//        ServletOutputStream sosRef = response.getOutputStream();
        String filePath = "";
        OutputStream ops = new BufferedOutputStream(filePath);
        try {
            JasperRunManager.runReportToPdfStream(isRef, sosRef, parameters, new JREmptyDataSource());
//            response.setContentType("application/pdf");
        } catch (JRException e) {

            e.printStackTrace();
        }finally {
            sosRef.flush();
            sosRef.close();
        }



    }
}
