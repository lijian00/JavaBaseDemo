package work.lijian4net.www.jasperreport;



import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;


import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
* @description 打印pdf
* @author yuyuqiang
* @date 2020/10/29
*/
public class JasperPdfReportUtil{

    /**
    * @description 打印base64
    * @param jrBeanCollectionDataSource：子集
    * @return
    * @author yuyuqiang
    * @date 2020/10/29
    */
    public static  String  exportReportToPdfStream(String jasperFilePath, Map<String,Object> parameters, JRDataSource jrBeanCollectionDataSource )  {
        String encodedText = "";
        try {
        /*    JRDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(mapList);*/
            File jasperFile = ResourceUtils.getFile(jasperFilePath);
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperFile);
            byte[] pdfStream = JasperRunManager.runReportToPdf(jasperReport, parameters, jrBeanCollectionDataSource);
            //文件流转换为base64格式
            final Base64.Encoder encoder = Base64.getEncoder();
            encodedText = encoder.encodeToString(pdfStream);
        } catch (IOException | JRException e) {
            e.printStackTrace();
        }
        return encodedText;
    }

    /**
     * 生成完成PDF
     * @param jasperPath jasper文件路径
     * @param destPath 生成pdf路径
     * @param destFileName 生成pdf文件名
     * @param parameters P参数
     * @param sceneFileList 现场证据集合
     * @param collectFileList 用采数据集合
     * @throws Exception
     */
    public static String generateAllPdf(String jasperPath,String destPath,String destFileName, Map<String,Object> parameters, List<Object> sceneFileList,List<Object> collectFileList) throws Exception {
        String cerfiticateJasperPath = jasperPath+File.separator+"Certificate.jasper";
        String bodyJasperPath = jasperPath+File.separator+"CerBody.jasper";
        String collectJasperPath = jasperPath+File.separator+"CerCollect.jasper";
        String certificatedestPdfPath =  destPath+File.separator+"certificate.pdf";
        String bodyDestPdfPath =  destPath+File.separator+"cerBody.pdf";
        String collectDestPdfPath =  destPath+File.separator+"cerCollect.pdf";
        //前两页
        JasperPdfReportUtil.generatePdfByJrxml(cerfiticateJasperPath,certificatedestPdfPath,parameters,null);

        //现场证据
        //转换成图片需要的数据
        Map<String, List<Object>> map = sceneFileList.stream().collect(Collectors.groupingBy(Object::getFileId));
        List<ResJapserBase> jasperBaseSceneImg = new ArrayList<>();
        for(String key:map.keySet()){
            ResJapserBase japserBase = new ResJapserBase();
            japserBase.setFileId1("imgs_"+map.get(key).get(0).getFileId());//区分现场证据和用采证据，避免证据id相同
            japserBase.setSceneFileTitle(map.get(key).get(0).getSceneFileTitle());
            japserBase.setLinkDate(map.get(key).get(0).getLinkDate());
            japserBase.setSceneId(map.get(key).get(0).getSceneId());
            japserBase.setBlockHash(map.get(key).get(0).getBlockHash());

            if(map.get(key).size()>0 && map.get(key).size()<=3){
                japserBase.setFileId2("");
            }else if(map.get(key).size()<=6){
                japserBase.setFileId2(japserBase.getFileId1());
            }
            List<byte[]> imgBytes = new ArrayList<>();
            for(ResSceneFile sceneFile :map.get(key)){
                imgBytes.add(sceneFile.getImageContent());
            }
            japserBase.setImgPathList(imgBytes);
            jasperBaseSceneImg.add(japserBase);
        }
        //转换成报表需要的信息
        List<ResJapserBase> jasperBaseSceneInfo = jasperBaseSceneImg.stream().collect(Collectors.
                collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(ResJapserBase:: getFileId1))), ArrayList::new));

        List<ResJasperSceneFile> jasperSceneFiles = new ArrayList<>();
        for(int i=0;i<jasperBaseSceneInfo.size();i++){
            ResJapserBase baseFile = jasperBaseSceneInfo.get(i);
            ResJasperSceneFile jasperSceneFile = new ResJasperSceneFile();
            jasperSceneFile.setFileId1(baseFile.getFileId1());
            jasperSceneFile.setFileId2(baseFile.getFileId2());
            jasperSceneFile.setSceneFileTitle((i+1)+"）"+baseFile.getSceneFileTitle());
            jasperSceneFile.setLinkDate(baseFile.getLinkDate());
            jasperSceneFile.setSceneId(baseFile.getSceneId());
            jasperSceneFile.setBlockHash(baseFile.getBlockHash());
            jasperSceneFile.setHashAlgorithm("MD5");
            jasperSceneFiles.add(jasperSceneFile);

        }
        JasperPdfReportUtil.generatePdfByJrxml(bodyJasperPath,bodyDestPdfPath,parameters,jasperSceneFiles);

        //用采证据
        //转换成图片需要的数据
        Map<String, List<ResCollectFile>> collectMap = collectFileList.stream().
                collect(Collectors.groupingBy(ResCollectFile::getFileId));
        List<ResJapserBase> jasperBaseCollectImg = new ArrayList<>();
        for(String key:collectMap.keySet()){
            ResJapserBase japserBase = new ResJapserBase();
            japserBase.setFileId1("imgc_"+collectMap.get(key).get(0).getFileId());
            japserBase.setCollectFileTitle(""+collectMap.get(key).get(0).getCollectFileTitle());
            japserBase.setCollectId(collectMap.get(key).get(0).getCollectId());
            japserBase.setLinkDate(collectMap.get(key).get(0).getLinkDate());
            japserBase.setBlockHash(collectMap.get(key).get(0).getBlockHash());
            List<byte[]> imgBytes = new ArrayList<>();
            for(ResCollectFile sceneFile :collectMap.get(key)){
                imgBytes.add(sceneFile.getImageContent());
            }
            japserBase.setImgPathList(imgBytes);
            jasperBaseCollectImg.add(japserBase);
        }
        //转换成报表需要的信息
        List<ResJapserBase> jasperBaseCollectInfo = jasperBaseCollectImg.stream().collect(Collectors.
                collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(ResJapserBase:: getFileId1))), ArrayList::new));

        List<ResJasperCollectFile> jasperCollectFiles = new ArrayList<>();
        for(int i=0;i<jasperBaseCollectInfo.size();i++){
            ResJapserBase baseFile = jasperBaseCollectInfo.get(i);
            ResJasperCollectFile jasperCollectFile = new ResJasperCollectFile();
            jasperCollectFile.setFileId1(baseFile.getFileId1());
            jasperCollectFile.setLinkDate(baseFile.getLinkDate());
            jasperCollectFile.setCollectId(baseFile.getCollectId());
            jasperCollectFile.setBlockHash(baseFile.getBlockHash());
            jasperCollectFile.setHashAlgorithm("MD5");
            jasperCollectFile.setCollectFileTitle((i+1)+"）"+((baseFile.getCollectFileTitle()==null || "null".equals(baseFile.getCollectFileTitle())) ? "":baseFile.getCollectFileTitle()));
            jasperCollectFiles.add(jasperCollectFile);
        }
        JasperPdfReportUtil.generatePdfByJrxml(collectJasperPath,collectDestPdfPath,parameters,jasperCollectFiles);

        //合并
        String[] files = {certificatedestPdfPath,bodyDestPdfPath,collectDestPdfPath};
        String destPDFPath = destPath+File.separator+destFileName+".pdf";
        JasperPdfReportUtil.mergePdfFiles(files,destPDFPath);
        new File(certificatedestPdfPath).delete();
        new File(bodyDestPdfPath).delete();
        new File(collectDestPdfPath).delete();
        jasperBaseSceneImg.addAll(jasperBaseCollectImg);
        String filepath = PDFUtil.writeToPdf(destFileName, jasperBaseSceneImg);
        return filepath;
    }

    /**
     * @description 打印base64
     * @param jasperFilePath：jasper文件路径
     * @param destPath：生成pdf路径
     * @param parameters：key-value参数
     * @param list：循环数据集合
     * @return
     * @author lijian
     * @date 2021/11/5
     */
    public static void generatePdfByJrxml(String jasperFilePath,String destPath, Map<String,Object> parameters, List<?> list) throws JRException {

        JRDataSource jrDataSource;
        if(list == null){
            jrDataSource = new JREmptyDataSource();
        }else{
            jrDataSource= new JRBeanCollectionDataSource(list);
        }
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperFilePath, parameters, jrDataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint,destPath);
    }


    /**
     * 合并pdf文件
     * @param files 要合并文件数组(绝对路径如{ "e:\\1.pdf", "e:\\2.pdf" ,
     * "e:\\3.pdf"}),合并的顺序按照数组中的先后顺序，如2.pdf合并在1.pdf后。
     * @param newfile 合并后新产生的文件绝对路径，如 e:\\temp\\tempNew.pdf,
     * @return boolean 合并成功返回true；否则，返回false
     *
     */

    public static boolean mergePdfFiles(String[] files, String newfile) {
        boolean retValue = false;
        Document document = null;
        PdfCopy copy = null;
        PdfReader reader = null;
        try {
            reader=new PdfReader(files[0]);
            document = new Document(reader.getPageSize(1));
            copy = new PdfCopy(document, new FileOutputStream(newfile));
            document.open();
            for (int i = 0; i < files.length; i++) {
                if(reader !=null) {
                    reader.close();
                }
                reader = new PdfReader(files[i]);
                int n = reader.getNumberOfPages();
                for (int j = 1; j <= n; j++) {
                    document.newPage();
                    PdfImportedPage page = copy.getImportedPage(reader, j);
                    copy.addPage(page);
                }
            }
            retValue = true;
        } catch (Exception e) {
            retValue = false;
            System.out.println(e);
        } finally {
            if(copy!=null){
                copy.close();
            }
            if(reader !=null){
                reader.close();
            }
            if(document !=null){
                document.close();
            }
        }
        return retValue;
    }


    /**
     * 将pdf文件转base64字节数组
     * @param sourceFile
     * @return
     */
    public static byte[] PDFToBase64(String sourceFile) {
        Base64Encoder encoder = new Base64Encoder();
        FileInputStream fin =null;
        BufferedInputStream bin =null;
        ByteArrayOutputStream baos = null;
        BufferedOutputStream bout =null;
        File file = new File(sourceFile);
        try {
            fin = new FileInputStream(file);
            bin = new BufferedInputStream(fin);
            baos = new ByteArrayOutputStream();
            bout = new BufferedOutputStream(baos);
            byte[] buffer = new byte[1024];
            int len = bin.read(buffer);
            while(len != -1){
                bout.write(buffer, 0, len);
                len = bin.read(buffer);
            }
            //刷新此输出流并强制写出所有缓冲的输出字节
            bout.flush();
            byte[] bytes = baos.toByteArray();
            return bytes;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                fin.close();
                bin.close();
                bout.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
