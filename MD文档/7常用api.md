1、Scanner

	/*
	Scanner类的功能：可以实现键盘输入数据，到程序当中。
	
	引用类型的一般使用步骤：
	
	1. 导包
	import 包路径.类名称;
	如果需要使用的目标类，和当前类位于同一个包下，则可以省略导包语句不写。
	只有java.lang包下的内容不需要导包，其他的包都需要import语句。
	
	2. 创建
	类名称 对象名 = new 类名称();
	
	3. 使用
	对象名.成员方法名()
	
	获取键盘输入的一个int数字：int num = sc.nextInt();
	获取键盘输入的一个字符串：String str = sc.next();
	 */
	public class Demo01Scanner {
	
	    public static void main(String[] args) {
	        // 2. 创建
	        // 备注：System.in代表从键盘进行输入
	        Scanner sc = new Scanner(System.in);
	
	        // 3. 获取键盘输入的int数字
	        int num = sc.nextInt();
	        System.out.println("输入的int数字是：" + num);
	
	        // 4. 获取键盘输入的字符串
	        String str = sc.next();
	        System.out.println("输入的字符串是：" + str);
	    }
	
	}
2、匿名对象

	创建对象的标准格式：
	类名称 对象名 = new 类名称();
	
	匿名对象就是只有右边的对象，没有左边的名字和赋值运算符。
	new 类名称();
	
	注意事项：匿名对象只能使用唯一的一次，下次再用不得不再创建一个新对象。
	使用建议：如果确定有一个对象只需要使用唯一的一次，就可以用匿名对象。

3、Random

	Random类用来生成随机数字。使用起来也是三个步骤：
	
	1. 导包
	import java.util.Random;
	
	2. 创建
	Random r = new Random(); // 小括号当中留空即可
	
	3. 使用
	获取一个随机的int数字（范围是int所有范围，有正负两种）：int num = r.nextInt()
	获取一个随机的int数字（参数代表了范围，左闭右开区间）：int num = r.nextInt(3)
	实际上代表的含义是：[0,3)，也就是0~2
4、数组

	数组有一个缺点：一旦创建，程序运行期间长度不可以发生改变。
	数组的长度不可以发生改变。
	但是ArrayList集合的长度是可以随意变化的。
	
	对于ArrayList来说，有一个尖括号<E>代表泛型。
	泛型：也就是装在集合当中的所有元素，全都是统一的什么类型。
	注意：泛型只能是引用类型，不能是基本类型。
	
	注意事项：
	对于ArrayList集合来说，直接打印得到的不是地址值，而是内容。
	如果内容是空，得到的是空的中括号：[]

	// 创建了一个ArrayList集合，集合的名称是list，里面装的全都是String字符串类型的数据
	// 备注：从JDK 1.7+开始，右侧的尖括号内部可以不写内容，但是<>本身还是要写的。
    ArrayList<String> list = new ArrayList<>();


	ArrayList当中的常用方法有：

	public boolean add(E e)：向集合当中添加元素，参数的类型和泛型一致。返回值代表添加是否成功。
	备注：对于ArrayList集合来说，add添加动作一定是成功的，所以返回值可用可不用。
	但是对于其他集合（今后学习）来说，add添加动作不一定成功。
	
	public E get(int index)：从集合当中获取元素，参数是索引编号，返回值就是对应位置的元素。
	
	public E remove(int index)：从集合当中删除元素，参数是索引编号，返回值就是被删除掉的元素。
	
	public int size()：获取集合的尺寸长度，返回值是集合中包含的元素个数。

	
	如果希望向集合ArrayList当中存储基本类型数据，必须使用基本类型对应的“包装类”。
	
	基本类型    包装类（引用类型，包装类都位于java.lang包下）
	byte        Byte
	short       Short
	int         Integer     【特殊】
	long        Long
	float       Float
	double      Double
	char        Character   【特殊】
	boolean     Boolean
	
	从JDK 1.5+开始，支持自动装箱、自动拆箱。
	
	自动装箱：基本类型 --> 包装类型
	自动拆箱：包装类型 --> 基本类型
5、String

	java.lang.String类代表字符串。
	API当中说：Java 程序中的所有字符串字面值（如 "abc" ）都作为此类的实例实现。
	其实就是说：程序当中所有的双引号字符串，都是String类的对象。（就算没有new，也照样是。）
	
	字符串的特点：
	1. 字符串的内容永不可变。【重点】
	2. 正是因为字符串不可改变，所以字符串是可以共享使用的。
	3. 字符串效果上相当于是char[]字符数组，但是底层原理是byte[]字节数组。
	
	创建字符串的常见3+1种方式。
	三种构造方法：
	public String()：创建一个空白字符串，不含有任何内容。
	public String(char[] array)：根据字符数组的内容，来创建对应的字符串。
	public String(byte[] array)：根据字节数组的内容，来创建对应的字符串。
	一种直接创建：
	String str = "Hello"; // 右边直接用双引号
	
	注意：直接写上双引号，就是字符串对象。
	字符串常量池：程序当中直接写上的双引号字符串，就在字符串常量池中。

	对于基本类型来说，==是进行数值的比较。
	对于引用类型来说，==是进行【地址值】的比较。

	==是进行对象的地址值比较，如果确实需要字符串的内容比较，可以使用两个方法：
	
	public boolean equals(Object obj)：参数可以是任何对象，只有参数是一个字符串并且内容相同的才会给true；否则返回false。
	注意事项：
	1. 任何对象都能用Object进行接收。
	2. equals方法具有对称性，也就是a.equals(b)和b.equals(a)效果一样。
	3. 如果比较双方一个常量一个变量，推荐把常量字符串写在前面。
	推荐："abc".equals(str)    不推荐：str.equals("abc")
	
	public boolean equalsIgnoreCase(String str)：忽略大小写，进行内容比较。

	String当中与获取相关的常用方法有：

	public int length()：获取字符串当中含有的字符个数，拿到字符串长度。
	public String concat(String str)：将当前字符串和参数字符串拼接成为返回值新的字符串。
	public char charAt(int index)：获取指定索引位置的单个字符。（索引从0开始。）
	public int indexOf(String str)：查找参数字符串在本字符串当中首次出现的索引位置，如果没有返回-1值。

	字符串的截取方法：
	public String substring(int index)：截取从参数位置一直到字符串末尾，返回新字符串。
	public String substring(int begin, int end)：截取从begin开始，一直到end结束，中间的字符串。
	备注：[begin,end)，包含左边，不包含右边。
	
	String当中与转换相关的常用方法有：

	public char[] toCharArray()：将当前字符串拆分成为字符数组作为返回值。
	public byte[] getBytes()：获得当前字符串底层的字节数组。
	public String replace(CharSequence oldString, CharSequence newString)：
	将所有出现的老字符串替换成为新的字符串，返回替换之后的结果新字符串。
	备注：CharSequence意思就是说可以接受字符串类型。

	分割字符串的方法：
	public String[] split(String regex)：按照参数的规则，将字符串切分成为若干部分。
	
	注意事项：
	split方法的参数其实是一个“正则表达式”，今后学习。
	今天要注意：如果按照英文句点“.”进行切分，必须写"\\."（两个反斜杠）
6、static
	
	如果一个成员变量使用了static关键字，那么这个变量不再属于对象自己，而是属于所在的类。多个对象共享同一份数据。

	一旦使用static修饰成员方法，那么这就成为了静态方法。静态方法不属于对象，而是属于类的。

	如果没有static关键字，那么必须首先创建对象，然后通过对象才能使用它。
	如果有了static关键字，那么不需要创建对象，直接就能通过类名称来使用它。
	
	无论是成员变量，还是成员方法。如果有了static，都推荐使用类名称进行调用。
	静态变量：类名称.静态变量
	静态方法：类名称.静态方法()
	
	注意事项：
	1. 静态不能直接访问非静态。
	原因：因为在内存当中是【先】有的静态内容，【后】有的非静态内容。
	“先人不知道后人，但是后人知道先人。”
	2. 静态方法当中不能用this。
	原因：this代表当前对象，通过谁调用的方法，谁就是当前对象。
	
	静态代码块的格式是：

	public class 类名称 {
	    static {
	        // 静态代码块的内容
	    }
	}
	
	特点：当第一次用到本类时，静态代码块执行唯一的一次。
	静态内容总是优先于非静态，所以静态代码块比构造方法先执行。
	
	静态代码块的典型用途：
	用来一次性地对静态成员变量进行赋值。

7、Arrays

	java.util.Arrays是一个与数组相关的工具类，里面提供了大量静态方法，用来实现数组常见的操作。

	public static String toString(数组)：将参数数组变成字符串（按照默认格式：[元素1, 元素2, 元素3...]）
	public static void sort(数组)：按照默认升序（从小到大）对数组的元素进行排序。
	
	备注：
	1. 如果是数值，sort默认按照升序从小到大
	2. 如果是字符串，sort默认按照字母升序
	3. 如果是自定义的类型，那么这个自定义的类需要有Comparable或者Comparator接口的支持。（今后学习）
8、Math

	java.util.Math类是数学相关的工具类，里面提供了大量的静态方法，完成与数学运算相关的操作。
	
	public static double abs(double num)：获取绝对值。有多种重载。
	public static double ceil(double num)：向上取整。
	public static double floor(double num)：向下取整。
	public static long round(double num)：四舍五入。
	
	Math.PI代表近似的圆周率常量（double）。
