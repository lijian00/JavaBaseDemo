package work.lijian4net.www;

import work.lijian4net.www.domain.ClassInfo;
import work.lijian4net.www.domain.SchoolInfo;
import work.lijian4net.www.domain.UserInfo;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) throws IllegalAccessException {
        List<ClassInfo> classInfoList = new ArrayList<>();
        for(int j=0;j<2;j++){
            ClassInfo classInfo = new ClassInfo();
            classInfo.setClassID("classID"+j);
            classInfo.setClassNo("classNo"+j);
            classInfo.setClassLeader("classLeader"+j);
            classInfo.setClassName("班级"+j);
            List<UserInfo> userList = new ArrayList<>();
            for(int i=0;i<3;i++){
                UserInfo user = new UserInfo();
                user.setUserID(("id"+i)+j);
                user.setUserNo(("userNo"+i)+j);
                user.setUserName(("userName"+i)+j);
                userList.add(user);
            }
            classInfo.setUserInfoList(userList);
            classInfoList.add(classInfo);
        }

        SchoolInfo schoolInfo = new SchoolInfo();
        schoolInfo.setSchoolID("123123123123");
        schoolInfo.setSchoolNo("852741");
        schoolInfo.setSchoolName("二中");
        schoolInfo.setSchoolLeader("校长");
        schoolInfo.setClassInfoList(classInfoList);
        testReflect(schoolInfo);
    }


    public static void testReflect(Object obj) throws IllegalAccessException {
        System.out.println("------------");
        if(obj instanceof ArrayList){
            System.out.println("对象是集合");
            ArrayList list = (ArrayList)obj;
            for(Object obj1 : list){
                System.out.println("集合对象的名字："+obj1.getClass().getSimpleName());
                //获取对象的属性
                Field[] fields = obj1.getClass().getDeclaredFields();
                for(Field field :fields){
                    String name = field.getName();
                    String typeName = field.getGenericType().getTypeName();
                    field.setAccessible(true);//不写不能访问private属性
                    Object o = field.get(obj1);
                    if(o instanceof ArrayList){
                        testReflect(o);
                    }else{
                        System.out.printf("属性名：%s，类型：%s，值：%s\n",name,typeName,o);
                    }
                }
            }
        }else{
            System.out.println("对象不是集合");
            System.out.println("对象的名字："+obj.getClass().getSimpleName());
            System.out.println("对象的类型："+obj.getClass().getTypeName());
            //获取对象的属性
            Field[] fields = obj.getClass().getDeclaredFields();
            for(Field field :fields){
                String name = field.getName();
                String typeName = field.getGenericType().getTypeName();
                field.setAccessible(true);//不写不能访问private属性
                Object o = field.get(obj);
                if(o instanceof ArrayList){
                    testReflect(o);
                }else{
                    System.out.printf("属性名：%s，类型：%s，值：%s\n",name,typeName,o);
                }
            }

        }
    }
}
