package work.lijian4net.www.domain;

import java.util.List;

public class ClassInfo {
    private String classID;
    private String classNo;
    private String className;
    private String classLeader;
    private List<UserInfo> userInfoList;

    public String getClassID() {
        return classID;
    }

    public void setClassID(String classID) {
        this.classID = classID;
    }

    public String getClassNo() {
        return classNo;
    }

    public void setClassNo(String classNo) {
        this.classNo = classNo;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getClassLeader() {
        return classLeader;
    }

    public void setClassLeader(String classLeader) {
        this.classLeader = classLeader;
    }

    public List<UserInfo> getUserInfoList() {
        return userInfoList;
    }

    public void setUserInfoList(List<UserInfo> userInfoList) {
        this.userInfoList = userInfoList;
    }

    @Override
    public String toString() {
        return "ClassInfo{" +
                "classID='" + classID + '\'' +
                ", classNo='" + classNo + '\'' +
                ", className='" + className + '\'' +
                ", classLeader='" + classLeader + '\'' +
                ", userInfoList=" + userInfoList +
                '}';
    }
}
