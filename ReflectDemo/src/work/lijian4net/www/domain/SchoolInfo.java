package work.lijian4net.www.domain;

import java.util.List;

public class SchoolInfo {
    private String schoolID;
    private String schoolNo;
    private String schoolName;
    private String schoolLeader;
    private List<ClassInfo> classInfoList;

    @Override
    public String toString() {
        return "SchoolInfo{" +
                "schoolID='" + schoolID + '\'' +
                ", schoolNo='" + schoolNo + '\'' +
                ", schoolName='" + schoolName + '\'' +
                ", schoolLeader='" + schoolLeader + '\'' +
                ", classInfoList=" + classInfoList +
                '}';
    }

    public String getSchoolID() {
        return schoolID;
    }

    public void setSchoolID(String schoolID) {
        this.schoolID = schoolID;
    }

    public String getSchoolNo() {
        return schoolNo;
    }

    public void setSchoolNo(String schoolNo) {
        this.schoolNo = schoolNo;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolLeader() {
        return schoolLeader;
    }

    public void setSchoolLeader(String schoolLeader) {
        this.schoolLeader = schoolLeader;
    }

    public List<ClassInfo> getClassInfoList() {
        return classInfoList;
    }

    public void setClassInfoList(List<ClassInfo> classInfoList) {
        this.classInfoList = classInfoList;
    }
}
