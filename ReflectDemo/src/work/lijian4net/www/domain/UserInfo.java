package work.lijian4net.www.domain;

public class UserInfo {

    private String userID;
    private String userName;
    private String userNo;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "userID='" + userID + '\'' +
                ", userName='" + userName + '\'' +
                ", userNo='" + userNo + '\'' +
                '}';
    }
}
