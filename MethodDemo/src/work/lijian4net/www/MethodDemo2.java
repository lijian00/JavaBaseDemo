package work.lijian4net.www;

public class MethodDemo2 {
    public static void main(String[] args) {
        //调用方法getSum //并接收方法计算后的结果，整数
        int sum = getSum();
        System.out.println(sum);
    }

    /* 定义计算1~100的求和方法 返回值类型，计算结果整数int 参数：没有不确定数据 */
    public static int getSum() {
        //定义变量保存求和
        int sum = 0;
        // 从1开始循环，到100结束
        for (int i = 1; i <= 100; i++) {
            sum = sum + i;
        }
        return sum;
    }
}
